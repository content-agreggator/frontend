import {Link, Redirect, useParams} from "react-router-dom";
import {loadQuery, useRelayEnvironment} from "react-relay";
import {Col, Descriptions, Divider, Layout, List, Row, Space} from "antd";
import React, {useState} from "react";

import { Typography } from 'antd';
import UserDetails, {UserDetailsQuery} from "./UserDetails";
import decodeJwt from "../../app/jwt";
import store from "store";
import UserActionsSection from "./UserActionsSection";
import graphql from "babel-plugin-relay/macro";
const { usePreloadedQuery  } = require('react-relay');
const { Title } = Typography;

const { Content } = Layout;

export const UserSectionQuery = graphql`
    query UserSectionQuery($userId: String!) {
        userOther(id: $userId) {
            id
            username
            role
            friends {
                username
            }
            status
        }
    }`;

function UserSection() {
    let {userId} = useParams()
    console.log(userId)

    const environment = useRelayEnvironment()
    const detailsInitialQueryRef = loadQuery(
        environment,
        UserSectionQuery,
        {userId: userId}
    );



    return (
        <>
        <UserSectionCall environment={environment} userId={userId} detailsInitialQuery={detailsInitialQueryRef}/>
        </>
    )
}


function UserSectionCall({environment, userId, detailsInitialQuery}) {
    let data = usePreloadedQuery(UserSectionQuery, detailsInitialQuery);

    const token = store.get('token')
    const user = decodeJwt(token)

    console.log(user)

    const [redirect, setRedirect] = useState(false)


    const userOther = data.userOther

    console.log("userOther", data.userOther)

    return (
        <>
            <RedirectToProfile redirect={redirect} setRedirect={setRedirect} user_id={user.user_id} friend_id={userId} />
            <Space direction={"vertical"} size="large" style={{width:"100%"}}>

                <Row justify="space-around">
                    <Layout>
                        <Content>
                            <Row>
                                <Col span={12}>
                                    <Layout>

                                        <Row>
                                            <Divider orientation="center">User Details</Divider>
                                            <UserDetails user={userOther}/>
                                        </Row>
                                    </Layout>
                                </Col>
                                <Col span={12}>
                                    <Layout>
                                        <Row justify="space-around">
                                                <Divider orientation="center">User actions</Divider>
                                                <UserActionsSection environment={environment} user={userOther}/>

                                        </Row>
                                        <Row justify="space-around">
                                            <Divider orientation="center">Friends list</Divider>
                                            <FriendsList friends={userOther.friends}/>

                                        </Row>

                                    </Layout>
                                </Col>
                            </Row>

                        </Content>

                    </Layout>


                </Row>
                <Row justify="space-around">

                    <Col span={24}>
                        <Layout>
                        </Layout>
                    </Col>

                </Row>

                <Row justify="space-around">

                    <Col span={24}>
                        <Layout>
                            {/*<Content style={{ padding: '5px'   }}><CommentListSection novelId={novelId}/></Content>*/}
                        </Layout>
                    </Col>

                </Row>
            </Space>

        </>
    )
}


function RedirectToProfile({redirect, setRedirect, user_id, friend_id}) {

    if (user_id === friend_id) {
        setRedirect(true)
    }

    return (
        <>
            {redirect === true ? <Redirect to={'/profile'}/> : <></>}
        </>
    )
}

function FriendsList({friends}) {

    console.log("friends ", friends)
    return (
        <>

            {
                friends?.length > 0 ?             <List
                    size="small"

                    dataSource={friends}
                    pagination={{
                        onChange: page => {
                            console.log(page);
                        },
                        pageSize: 10,
                    }}
                    renderItem={item => (
                        <List.Item>
                            {item.userName} {item.role} <Link to={"/user/"+ item.userId} >Enter</Link>
                        </List.Item>
                    )}
                /> : <></> }
        </>
    )
}

export default UserSection