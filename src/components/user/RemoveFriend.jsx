import graphql from "babel-plugin-relay/macro";
import {commitMutation} from "react-relay";

const RemoveFriendMutation = graphql`
    mutation RemoveFriendMutation($friendId: ID!) {
        removeFriend(friendId: $friendId)
    }`;

export function commitRemoveFriendMutation(
    environment,
    input,
) {

    return commitMutation(environment, {
        mutation: RemoveFriendMutation,
        variables: input,
        onCompleted: response => {
            console.log(response)
            window.location.reload();
        },
        onError: error => {console.error(error)},
    })


}
