import graphql from "babel-plugin-relay/macro";
import {commitMutation} from "react-relay";

const BanUserMutation = graphql`
    mutation BanUserMutation($userId: ID!) {
        banUser(userId: $userId) 
    }`;

export function commitBanUserMutation(
    environment,
    input,
) {

    return commitMutation(environment, {
        mutation: BanUserMutation,
        variables: input,
        onCompleted: response => {
            console.log(response)
            window.location.reload();
        },
        onError: error => {console.error(error)},
    })


}
