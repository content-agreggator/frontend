import React, {useEffect} from "react";
import {useParams} from "react-router-dom";
import store from "store";
import decodeJwt from "../../app/jwt";
import {Button} from "antd";
import {loadQuery} from "react-relay";
import {commitAddFriendMutation} from "./AddFriend";
import {commitRemoveFriendMutation} from "./RemoveFriend";
import graphql from "babel-plugin-relay/macro";
import {commitBanUserMutation} from "./BanUser";
import {commitUnbanUserMutation} from "./UnbanUser";
const { usePreloadedQuery   } = require('react-relay');

const UserActionsSectionQuery = graphql`
    query UserActionsSectionQuery($userId: ID!) {
        isUserAFriend(userId: $userId)
    }`;

function UserActionsSection({environment, user}) {
    console.log(11, user)
    let {userId} = useParams()

    const isFriendInitialQueryRef = loadQuery(
        environment,
        UserActionsSectionQuery,
        {userId: userId}
    );
    //
    // const token = store.get('token')
    // const user = decodeJwt(token)
    //
    // console.log(user)

    return (
        <>
                 <AddRemoveFriendCall environment={environment} userId={userId} isFriendQuery={isFriendInitialQueryRef}/>
                 <BanUnbanCall environment={environment} userOther={user}  />
        </>
    )
}

function AddRemoveFriendCall({isFriendQuery ,environment, userId}) {

    const data = usePreloadedQuery(UserActionsSectionQuery, isFriendQuery)

    console.log(1, data)

    console.log(userId)

    const token = store.get('token')
    const user = decodeJwt(token)

    console.log(user)

    const addFriend = () => {
        commitAddFriendMutation(environment, {friendId: userId})
    }
    const removeFriend = () => {
        commitRemoveFriendMutation(environment, {friendId: userId})

    }

    return (
        <>
            { !data.isUserAFriend  ?
                <Button onClick={addFriend} >Add friend</Button>
                :  <Button onClick={removeFriend} >Remove friend</Button>

            }

        </>
    )
}


function BanUnbanCall({environment, userOther}) {

    const token = store.get('token')
    const user = decodeJwt(token)

    console.log("user", user)


    console.log("213", userOther)

    const banUser = () => {
        commitBanUserMutation(environment, {userId: userOther.id})
    }
    const unbanUser = () => {
        commitUnbanUserMutation(environment, {userId: userOther.id})

    }

    return (
        <>

            { user.Role == "Admin" ?
                <>
                    <Button onClick={banUser} >Ban user</Button>
                    <Button onClick={unbanUser} >Unban user</Button>
                </>    : <></> }
            {/*{ !data.isUserAFriend  ?*/}
            {/*    <Button onClick={banUser} >Ban user</Button>*/}
            {/*    :  <Button onClick={unbanUser} >Unban user</Button>*/}

            {/*}*/}

        </>
    )
}


export default UserActionsSection
