import graphql from "babel-plugin-relay/macro";
import {commitMutation} from "react-relay";

const AddFriendMutation = graphql`
    mutation AddFriendMutation($friendId: ID!) {
        addFriend(friendId: $friendId)
    }`;

export function commitAddFriendMutation(
    environment,
    input,
) {

    return commitMutation(environment, {
        mutation: AddFriendMutation,
        variables: input,
        onCompleted: response => {
            console.log(response)
            window.location.reload();
        },
        onError: error => {console.error(error)},
    })


}
