import React from "react";
import { Descriptions } from 'antd';
import { Typography } from 'antd';
import {Content} from "antd/es/layout/layout";
import graphql from "babel-plugin-relay/macro";

const { usePreloadedQuery } = require('react-relay');
const { Title } = Typography;



function UserDetails({user}) {
    return (
        <>
            <Descriptions title="" layout="horizontal" bordered column={1}>
                <Descriptions.Item label="Username">{user.username}</Descriptions.Item>
                {/*<Descriptions.Item label="Email">{user.email}</Descriptions.Item>*/}
                <Descriptions.Item label="Role">{user.role}</Descriptions.Item>
                <Descriptions.Item label="Status">{user.status}</Descriptions.Item>
                {/*<Descriptions.Item label="Rating">{novel.rating.avg +"  "+ novel.rating.count}</Descriptions.Item>*/}
            </Descriptions>
        </>
    )
}

export default UserDetails