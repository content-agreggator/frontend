import graphql from "babel-plugin-relay/macro";
import {commitMutation} from "react-relay";

const UnbanUserMutation = graphql`
    mutation UnbanUserMutation($userId: ID!) {
        unbanUser(userId: $userId) 
    }`;

export function commitUnbanUserMutation(
    environment,
    input,
) {

    return commitMutation(environment, {
        mutation: UnbanUserMutation,
        variables: input,
        onCompleted: response => {
            console.log(response)
            window.location.reload();
        },
        onError: error => {console.error(error)},
    })


}
