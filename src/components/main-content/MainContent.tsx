import {Breadcrumb, Layout} from "antd";
import {Route, Switch} from "react-router-dom";
import MultimediaTable from "../multimedia-table/MultimediaTable";
import MultimediaDetails from "../multimedia-details/MultimediaDetails";
import React from "react";
import {ErrorBoundary} from 'react-error-boundary'
import { useLocation,Link } from 'react-router-dom'
import Login from "../auth/Login";
import Logout from "../auth/Logout";
import Register from "../auth/Register";
import UserSection from "../user/UserSection";
import ProfileSection from "../profile/ProfileSection";
import NewNovel from "../multimedia-details/AddMultimedia";
import NewNovelSection from "../multimedia-details/AddMultimedia";
import AddMultimediaSection from "../multimedia-details/AddMultimedia";
import CommunityTable from "../community-section/CommunityTable";
import AddCommunitySection from "../community-section/AddCommunity";
import CommunityDetails from "../community-section/CommunityDetails";

const { Suspense } = React;
const {  Content } = Layout;

function ErrorFallback({error, resetErrorBoundary}) {
    return (
        <div role="alert">
            <p>Something went wrong:</p>
            <pre>{error.message}</pre>
            <button onClick={resetErrorBoundary}>Try again</button>
        </div>
    )
}

function MainContent() {
    const location = useLocation();
    const path = location.pathname.replace('-',' ');
    const paths = path.split('/');

    let items = []
    for (var i = 1; i < paths.length; i++) {
        // @ts-ignore
        items.push(<Breadcrumb.Item>{paths[i]} </Breadcrumb.Item>);
    }


    return (
        <>
            <Layout>
                <Layout style={{ padding: '0 24px 24px' }}>
                    <Breadcrumb style={{ textAlign: "left",  margin: '16px 0', textTransform: "capitalize"}}>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        {items}
                    </Breadcrumb>
                    <Content
                        className="site-layout-background"
                        style={{
                            padding: 24,
                            margin: "auto",
                            minHeight: 1000,
                            width: "80%",
                            justifyContent: "center",
                            alignItems: "stretch",
                            alignContent: "center"
                        }}
                    >
                        <ErrorBoundary FallbackComponent={ErrorFallback}>
                            <Suspense fallback={"loading"}>
                                    <Switch>
                                        {/*Multimedia section*/}
                                        <Route path="/multimedia-list">
                                            <MultimediaTable />
                                        </Route>
                                        <Route path="/multimedia-details/:novelId" >
                                            <MultimediaDetails/>
                                        </Route>
                                        <Route path="/new-multimedia">
                                            <AddMultimediaSection/>
                                        </Route>

                                        {/*User section*/}
                                        <Route path="/login">
                                            <Login/>
                                        </Route>
                                        <Route path="/register">
                                            <Register/>
                                        </Route>
                                        <Route path="/logout">
                                            <Logout/>
                                        </Route>
                                        <Route path="/user/:userId">
                                            <UserSection/>
                                        </Route>
                                        <Route path="/profile">
                                            <ProfileSection/>
                                        </Route>

                                        {/*Community section*/}
                                        <Route path="/community-list">
                                            <CommunityTable/>
                                        </Route>
                                        <Route path="/new-community">
                                            <AddCommunitySection/>
                                        </Route>
                                        <Route path="/community-details/:communityId">
                                            <CommunityDetails/>
                                        </Route>


                                        <Route path="/">

                                        </Route>
                                    </Switch>
                            </Suspense>
                        </ErrorBoundary>
                    </Content>
                </Layout>
            </Layout>
        </>
    )
}

export default MainContent