import { Form, Input, Button, Checkbox } from 'antd';
import graphql from "babel-plugin-relay/macro";
import { useRelayEnvironment,commitMutation} from "react-relay";
import { Redirect } from 'react-router-dom';
import React, { useState } from 'react';

var store = require('store')

const {commitLocalUpdate} = require('react-relay');


function commitCommentCreateLocally(
    environment,
) {
    return commitLocalUpdate(environment, store => {
        store.invalidateStore();
    });
}



function Logout() {
    store.remove('token')

    const environment = useRelayEnvironment()
    commitCommentCreateLocally(environment)

    return (
        <>
            <Redirect to={'/'}/>>}

</>

    )
}

export default Logout