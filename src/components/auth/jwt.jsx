const {commitLocalUpdate, graphql} = require('react-relay');


export default function commitSetTokenLocally(
    environment,
    token
) {
    return commitLocalUpdate(environment, store => {
       const   createdToken =  store.create(token,  "token"  );
    })
}

