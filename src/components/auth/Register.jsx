import { Form, Input, Button, Checkbox } from 'antd';
import graphql from "babel-plugin-relay/macro";
import { useRelayEnvironment,commitMutation} from "react-relay";
import { Redirect } from 'react-router-dom';
import React, { useState } from 'react';


const RegisterMutation = graphql`
    mutation RegisterMutation($email: String!, $username: String!, $password: String!) {
        register(input: {email: $email, username: $username, password: $password}) {
            username
        }
    }`;

function commitRegisterMutation(
    environment,
    input,
    setRedirect,
) {

    return commitMutation(environment, {
        mutation: RegisterMutation,
        variables: input,
        onCompleted: response => {
            setRedirect(true)
        },
        onError: error => {console.error(error)},
    })
}


function Register() {
    const [form] = Form.useForm();


    const environment = useRelayEnvironment()

    const [redirect, setRedirect] = useState(false)

    const onFinish = (values) => {
        commitRegisterMutation(environment, {email: values.email, username: values.username, password: values.password}, setRedirect);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    return (
        <>
            {redirect === true ? <Redirect to={'/login'}/> : <></>}

            <Form
                form={form}
                name="register"
                onFinish={onFinish}
                initialValues={{
                    residence: ['zhejiang', 'hangzhou', 'xihu'],
                    prefix: '86',
                }}
                scrollToFirstError
            >
                <Form.Item
                    name="email"
                    label="E-mail"
                    rules={[
                        {
                            type: 'email',
                            message: 'The input is not valid E-mail!',
                        },
                        {
                            required: true,
                            message: 'Please input your E-mail!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    name="password"
                    label="Password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                    hasFeedback
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    name="confirm"
                    label="Confirm Password"
                    dependencies={['password']}
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: 'Please confirm your password!',
                        },
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                                if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve();
                                }
                                return Promise.reject(new Error('The two passwords that you entered do not match!'));
                            },
                        }),
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    name="username"
                    label="Username"
                    tooltip="What do you want others to call you?"
                    rules={[{ required: true, message: 'Please input your username!', whitespace: true }]}
                >
                    <Input />
                </Form.Item>

                {/*<Form.Item*/}
                {/*    name="residence"*/}
                {/*    label="Habitual Residence"*/}
                {/*    rules={[*/}
                {/*        { type: 'array', required: true, message: 'Please select your habitual residence!' },*/}
                {/*    ]}*/}
                {/*>*/}
                {/*    <Cascader options={residences} />*/}
                {/*</Form.Item>*/}

                {/*<Form.Item*/}
                {/*    name="phone"*/}
                {/*    label="Phone Number"*/}
                {/*    rules={[{ required: true, message: 'Please input your phone number!' }]}*/}
                {/*>*/}
                {/*    <Input addonBefore={prefixSelector} style={{ width: '100%' }} />*/}
                {/*</Form.Item>*/}

                {/*<Form.Item*/}
                {/*    name="donation"*/}
                {/*    label="Donation"*/}
                {/*    rules={[{ required: true, message: 'Please input donation amount!' }]}*/}
                {/*>*/}
                {/*    <InputNumber addonAfter={suffixSelector} style={{ width: '100%' }} />*/}
                {/*</Form.Item>*/}

                {/*<Form.Item*/}
                {/*    name="website"*/}
                {/*    label="Website"*/}
                {/*    rules={[{ required: true, message: 'Please input website!' }]}*/}
                {/*>*/}
                {/*    <AutoComplete options={websiteOptions} onChange={onWebsiteChange} placeholder="website">*/}
                {/*        <Input />*/}
                {/*    </AutoComplete>*/}
                {/*</Form.Item>*/}

                {/*<Form.Item*/}
                {/*    name="gender"*/}
                {/*    label="Gender"*/}
                {/*    rules={[{ required: true, message: 'Please select gender!' }]}*/}
                {/*>*/}
                {/*    <Select placeholder="select your gender">*/}
                {/*        <Option value="male">Male</Option>*/}
                {/*        <Option value="female">Female</Option>*/}
                {/*        <Option value="other">Other</Option>*/}
                {/*    </Select>*/}
                {/*</Form.Item>*/}

                {/*<Form.Item label="Captcha" extra="We must make sure that your are a human.">*/}
                {/*    <Row gutter={8}>*/}
                {/*        <Col span={12}>*/}
                {/*            <Form.Item*/}
                {/*                name="captcha"*/}
                {/*                noStyle*/}
                {/*                rules={[{ required: true, message: 'Please input the captcha you got!' }]}*/}
                {/*            >*/}
                {/*                <Input />*/}
                {/*            </Form.Item>*/}
                {/*        </Col>*/}
                {/*        <Col span={12}>*/}
                {/*            <Button>Get captcha</Button>*/}
                {/*        </Col>*/}
                {/*    </Row>*/}
                {/*</Form.Item>*/}

                {/*<Form.Item*/}
                {/*    name="agreement"*/}
                {/*    valuePropName="checked"*/}
                {/*    rules={[*/}
                {/*        {*/}
                {/*            validator: (_, value) =>*/}
                {/*                value ? Promise.resolve() : Promise.reject(new Error('Should accept agreement')),*/}
                {/*        },*/}
                {/*    ]}*/}
                {/*    {...tailFormItemLayout}*/}
                {/*>*/}
                {/*    <Checkbox>*/}
                {/*        I have read the <a href="">agreement</a>*/}
                {/*    </Checkbox>*/}
                {/*</Form.Item>*/}
                <Form.Item >
                    <Button type="primary" htmlType="submit">
                        Register
                    </Button>
                </Form.Item>
            </Form>

</>

    )
}

export default Register