import { Form, Input, Button, Checkbox } from 'antd';
import graphql from "babel-plugin-relay/macro";
import { useRelayEnvironment,commitMutation} from "react-relay";
import { Redirect, Link } from 'react-router-dom';
import React, { useState } from 'react';
import {LockOutlined, UserOutlined} from "@ant-design/icons";

var store = require('store')


const LoginMutation = graphql`
    mutation LoginMutation($email: String!, $password: String!) {
        login(email: $email, password: $password) {
            token
            expiredAt
        }
    }`;

function commitLoginMutation(
    environment,
    input,
    setRedirect,
) {

    return commitMutation(environment, {
        mutation: LoginMutation,
        variables: input,
        onCompleted: response => {
            store.set('token', response.login.token)
            setRedirect(true)
        },
        onError: error => {console.error(error)},
    })


}


function Login() {
    const environment = useRelayEnvironment()

    const [redirect, setRedirect] = useState(false)

    const onFinish = (values) => {
      commitLoginMutation(environment, {email: values.email, password: values.password}, setRedirect);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    return (
        <>
            {redirect === true ? <Redirect to={'/'}/> : <></>}

            <Form
                name="normal_login"
                className="login-form"
                initialValues={{ remember: true }}
                onFinish={onFinish}
            >
                <Form.Item
                    name="email"
                    rules={[{ required: true, message: 'Please input your e-mail!' }]}
                >
                    <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[{ required: true, message: 'Please input your Password!' }]}
                >
                    <Input
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        placeholder="Password"
                    />
                </Form.Item>
                <Form.Item>
                    <Form.Item name="remember" valuePropName="checked" noStyle>
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item>

                    <a className="login-form-forgot" href="">
                        Forgot password
                    </a>
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Log in
                    </Button>
                    Or <Link to="/register">register now!</Link>
                </Form.Item>
            </Form>

</>

    )
}

export default Login