import React, {useCallback} from "react";
import {Comment, Tooltip, Form, Button, List, Input, Divider} from 'antd';
import moment from 'moment';
import graphql from "babel-plugin-relay/macro";
import commitAddCommentToNovelMutation from "./AddCommentToNovel";


const { useQueryLoader, usePreloadedQuery,  useRelayEnvironment ,  loadQuery } = require('react-relay');
const { TextArea } = Input;

const CommentListSectionQuery = graphql`
    query CommentListSectionQuery($novelId: ID!) {
        comments(novelId: $novelId)  {
            id
            text
            author
            createdAt
        }
    }`;



const data = [
    {
        author: 'Han Solo',
        avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
        content: (
            <p>
                We supply a series of design principles, practical patterns and high quality design
                resources (Sketch and Axure), to help people create their product prototypes beautifully and
                efficiently.
            </p>
        ),
        datetime: (
            <Tooltip title={moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss')}>
                <span>{moment().subtract(1, 'days').fromNow()}</span>
            </Tooltip>
        ),
    },
    {
        author: 'Han Solo',
        avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
        content: (
            <p>
                We supply a series of design principles, practical patterns and high quality design
                resources (Sketch and Axure), to help people create their product prototypes beautifully and
                efficiently.
            </p>
        ),
        datetime: (
            <Tooltip title={moment().subtract(2, 'days').format('YYYY-MM-DD HH:mm:ss')}>
                <span>{moment().subtract(2, 'days').fromNow()}</span>
            </Tooltip>
        ),
    },
];

function CommentListSection({novelId}) {
    const environment = useRelayEnvironment()
    const initialQueryRef = loadQuery(
        environment,
        CommentListSectionQuery,
        {novelId: novelId},
    );

    const [
        queryReference,
        refreshComments,
    ] = useQueryLoader(
        CommentListSectionQuery,
        initialQueryRef, /* e.g. provided by router */
    );


    const onFinish = (values) => {
        if (values.text != null) {
            commitAddCommentToNovelMutation(environment, {commentDraft: {text: values.text}, novelId: novelId})
        }
    }

    let onChange = e => {
    };


    const [form] = Form.useForm();

    return (
        <>
            <Divider orientation="left">Comments</Divider>
            {novelId != null ?
                <CommentsList initialQueryRef={queryReference}/>
                : null}

            <Form
                form={form}
                name="addComent"
                onFinish={onFinish}
                initialValues={{
                }}
                scrollToFirstError
            >
                <Form.Item name="text">
                    <TextArea
                        placeholder={"Write comment"}
                        showCount
                        rows={4}
                        onChange={onChange}
                    />
                </Form.Item>
                <Form.Item>
                    <Button htmlType="submit" loading={false} onClick={onFinish} type="primary">
                        Add Comment
                    </Button>
                </Form.Item>
            </Form>        </>
    )
}


function CommentsList({initialQueryRef}) {
    let data = usePreloadedQuery(CommentListSectionQuery, initialQueryRef );

    return (
        <>
            <List
                className="comment-list"
                itemLayout="horizontal"
                dataSource={data.comments}
                renderItem={item => (
                    <li>
                        <Comment
                            author={item.author}
                            // avatar={item.avatar}
                            content={item.text}
                            datetime={item.createdAt}
                        />
                    </li>
                )}
            />
        </>
    )
}

export default CommentListSection