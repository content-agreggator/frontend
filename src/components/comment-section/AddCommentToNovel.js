import graphql from "babel-plugin-relay/macro";
import {commitMutation} from "react-relay";

const AddCommentToNovelMutation = graphql`
    mutation AddCommentToNovelMutation($commentDraft: CommentDraft!, $novelId:ID!) {
        addCommentToNovel(draft: $commentDraft, novelId: $novelId) {
            id
            text
            author
            createdAt
        }
    }`;



function commitAddCommentToNovelMutation(
    environment,
    input
) {
    console.log("input: ",input)

    return commitMutation(environment, {
        mutation: AddCommentToNovelMutation,
        variables: input,
        onCompleted: response => {
            window.location.reload();
        },
        onError: error => {console.error(error)},
    })
}



export default commitAddCommentToNovelMutation
