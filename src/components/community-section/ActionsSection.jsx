import graphql from "babel-plugin-relay/macro";
import {loadQuery, usePreloadedQuery} from "react-relay";
import {Button} from "antd";
import React from "react";
import {commitAddCommunitySectionMutation, commitJoinCommunityMutation} from "./JoinCommunity";
import {commitLeaveCommunityMutation} from "./LeaveCommunity";

const ActionsSectionQuery = graphql`
    query ActionsSectionQuery($communityId: ID!) {
        isUserInCommunity(communityId: $communityId)
    }`;

function ActionsSection({environment, communityId}) {

    const initialQueryRef = loadQuery(
        environment,
        ActionsSectionQuery,
        {communityId: communityId}
    );

    console.log(initialQueryRef)





    return (
        <>
            {initialQueryRef != null ?
                <JoinSectionCall environment={environment} communityId={communityId} queryReference={initialQueryRef}/>
                : null}

        </>
    )
}

function JoinSectionCall({environment, communityId, queryReference}) {
    let data = usePreloadedQuery(ActionsSectionQuery, queryReference );
    console.log(1, data)

    const joinCommunity = () => {
        commitJoinCommunityMutation(environment, {communityId: communityId})
    }
    const leaveCommunity = () => {
        commitLeaveCommunityMutation(environment, {communityId: communityId})

    }

    return (
        <>
            { !data.isUserInCommunity ?
                <Button onClick={joinCommunity} >Join community</Button>
                :  <Button onClick={leaveCommunity} >Leave community</Button>
            }



        </>
    )
}

export default ActionsSection