import graphql from "babel-plugin-relay/macro";
import {commitMutation} from "react-relay";

const LeaveCommunityMutation = graphql`
    mutation LeaveCommunityMutation($communityId: ID!) {
        LeaveCommunity(CommunityId: $communityId) {
            communityId
            userId
        }
    }`;

export function commitLeaveCommunityMutation(
    environment,
    input,
) {

    return commitMutation(environment, {
        mutation: LeaveCommunityMutation,
        variables: input,
        onCompleted: response => {
            console.log(response)
            window.location.reload();
        },
        onError: error => {console.error(error)},
    })


}
