import {Space, Divider, Button, List} from 'antd';
import { Row, Col } from 'antd';
import { Layout, Breadcrumb } from 'antd';
import CommentListSection from "../comment-section/CommentListSection";
import { Descriptions, Badge } from 'antd';
import { Typography } from 'antd';
import React from "react";
import graphql from "babel-plugin-relay/macro";
import {Link, useParams} from "react-router-dom";
import ActionsSection from "./ActionsSection";
const { usePreloadedQuery,  useRelayEnvironment ,  loadQuery } = require('react-relay');
const { Title } = Typography;

const { Content } = Layout;


const CommunityDetailsQuery = graphql`
    query CommunityDetailsQuery($id: ID!) {
        community(id: $id) {
            id
            name
            description
            users {
                userId
                userName
                role
            }
        }
    }`;




function CommunityDetails() {
    // @ts-ignore
    let {communityId} = useParams()


    const environment = useRelayEnvironment()
    const initialQueryRef = loadQuery(
        environment,
        CommunityDetailsQuery,
        {id: communityId}
    );

    console.log(initialQueryRef)




    return (
        <>
            <CommunityDetailsCall environment={environment} communityId={communityId} queryReference={initialQueryRef}/>


        </>
)
}

function CommunityDetailsCall({environment, communityId, queryReference}) {
    let data = usePreloadedQuery(CommunityDetailsQuery, queryReference);

    const community = data.community

    return (
        <>
            <Space direction={"vertical"} size="large" style={{width:"100%"}}>

                <Row justify="space-around">
                    <Layout>
                        <Content>
                            <Row>
                                <Col span={12}>
                                    <Layout>
                                        <Divider orientation="center">Community Info</Divider>
                                        <CommunityInfo community={data.community}/>
                                    </Layout>
                                </Col>
                                <Col span={12}>
                                    <Layout>
                                        <Row justify="space-around">
                                            <Divider orientation="center">Community actions</Divider>
                                            <ActionsSection environment={environment} communityId={communityId}/>
                                        </Row>
                                        <Row justify="space-around">
                                            <Divider orientation="center">User list</Divider>
                                            <CommunityUsers users={community.users}/>

                                        </Row>
                                    </Layout>
                                </Col>
                            </Row>

                        </Content>

                    </Layout>


                </Row>
                <Row justify="space-around">

                    <Col span={24}>
                        <Layout>
                        </Layout>
                    </Col>

                </Row>

                {/*<Row justify="space-around">*/}

                {/*    <Col span={24}>*/}
                {/*        <Layout>*/}
                {/*            <Content style={{ padding: '5px'   }}><CommentListSection novelId={communityId}/></Content>*/}
                {/*        </Layout>*/}
                {/*    </Col>*/}

                {/*</Row>*/}
            </Space>
        </>
    )
}


function CommunityInfo({community}) {

    console.log(community)
    return (
        <>
            <Descriptions title="" layout="horizontal" bordered column={1}>
                <Descriptions.Item label="Name">{community.name}</Descriptions.Item>
                <Descriptions.Item label="Description">{community.description}</Descriptions.Item>
            </Descriptions>
        </>
    )
}


function CommunityUsers({users}) {

    console.log(users)
    return (
        <>
            {/*<Descriptions title="" layout="horizontal" bordered column={1}>*/}
            {/*    <Descriptions.Item label="Name">{community.name}</Descriptions.Item>*/}
            {/*    <Descriptions.Item label="Description">{community.description}</Descriptions.Item>*/}
            {/*</Descriptions>*/}
            {
                users?.length > 0 ?             <List
                    size="small"

                    dataSource={users}
                    pagination={{
                        onChange: page => {
                            console.log(page);
                        },
                        pageSize: 10,
                    }}
                    renderItem={item => (
                        <List.Item>
                            {item.userName} {item.role} <Link to={"/user/"+ item.userId} >Enter</Link>
                        </List.Item>
                    )}
                /> : <></> }
        </>
    )
}


export default CommunityDetails