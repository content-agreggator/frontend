import { Table, Button } from 'antd';
import React from "react";
import {
    Link, Redirect
} from "react-router-dom";

import graphql from 'babel-plugin-relay/macro';
import decodeJwt from "../../app/jwt";
let store = require('store')

const { usePreloadedQuery, useQueryLoader,  useRelayEnvironment ,  loadQuery } = require('react-relay');


const CommunityTableQuery = graphql`
    query CommunityTableQuery {
        communitiesConnection {
            edges {
                cursor
                node {
                    id
                    name
                    description
                }
            }
            pageInfo {
                hasNextPage
                hasPreviousPage
            }
        }
    }`;



const columns = [
    {
        title: 'Id',
        dataIndex: ['node', 'id'],
        key: 'id',
    },
    {
        title: 'name',
        dataIndex: ['node', 'name'],
        key: 'name',
    },
    {
        title: 'Action',
        dataIndex: '',
        key: 'x',
        render:  record  => (
            <>
                <Link to={"/community-details/" + record.cursor}>Enter</Link>
            </>
        )
    },
];


function CommunityTable() {
    const environment = useRelayEnvironment()
    const initialQueryRef = loadQuery(
        environment,
        CommunityTableQuery,
        {}
    );

    const [queryReference, loaderQuery] = useQueryLoader(
        CommunityTableQuery,
        initialQueryRef
    );

    return (
        <>
            {queryReference != null ?
                <Communities queryReference={queryReference}/>
                : null}


        </>


    )
}
function Communities({queryReference}) {
    let data = usePreloadedQuery(CommunityTableQuery, queryReference );

    console.log(1, data)
    // const token = store.get('token')
    // const user = decodeJwt(token)

    // console.log(user)


    return (
        <>
            <Table
                rowKey={record => record.cursor}
                columns={columns}
                dataSource={data.communitiesConnection.edges}
                expandable={{
                    expandedRowRender: record => <p style={{ margin: 0 }}><code> {record.node?.description} </code> </p>,
                    rowExpandable: record => record.cursor !== "",
                }}
            />
            {/*{user.Role === "RoleAdmin" ?*/}
                <Button href={"/new-community"}>Add new community</Button>
            {/*}*/}

        </>
    );
}




export default CommunityTable