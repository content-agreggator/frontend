import graphql from "babel-plugin-relay/macro";
import {commitMutation} from "react-relay";

const JoinCommunityMutation = graphql`
    mutation JoinCommunityMutation($communityId: ID!) {
        JoinCommunity(CommunityId: $communityId) {
            communityId
            userId
        }
    }`;

export function commitJoinCommunityMutation(
    environment,
    input,
) {

    return commitMutation(environment, {
        mutation: JoinCommunityMutation,
        variables: input,
        onCompleted: response => {
            console.log(response)
            window.location.reload();
        },
        onError: error => {console.error(error)},
    })


}
