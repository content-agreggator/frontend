import {Link, Redirect} from "react-router-dom";
import {Button, Checkbox, Form, Input} from "antd";
import React, {useState} from "react";
import {commitMutation, useRelayEnvironment} from "react-relay";
import graphql from "babel-plugin-relay/macro";
import store from "store";

const AddCommunitySectionMutation = graphql`
    mutation AddCommunitySectionMutation($title: String!, $description: String!) {
        addCommunity(draft: {title: $title, description: $description }) 
    }`;

function commitAddCommunitySectionMutation(
    environment,
    input,
    setRedirect,
) {

    return commitMutation(environment, {
        mutation: AddCommunitySectionMutation,
        variables: input,
        onCompleted: response => {
            console.log(response)
            setRedirect(true)
        },
        onError: error => {console.error(error)},
    })


}

function AddCommunitySection() {
    const environment = useRelayEnvironment()

    const [redirect, setRedirect] = useState(false)

    const onFinish = (values) => {
        commitAddCommunitySectionMutation(environment, {title: values.title, description: values.description, authors: [], genres: [] }, setRedirect);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <>
            {redirect === true ? <Redirect to={'/community-list'}/> : <></>}

            <Form
                name="normal_login"
                className="new-novel-form"
                initialValues={{ remember: true }}
                onFinish={onFinish}
            >
                <Form.Item
                    name="title"
                    rules={[{ required: true, message: 'Please input novel title' }]}
                >
                    <Input placeholder='Title' />
                </Form.Item>
                <Form.Item
                    name="description"
                    rules={[{ required: true, message: 'Please input novel description!' }]}
                >
                    <Input
                        type="description"
                        placeholder="Description"
                    />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Create novel
                    </Button>
                </Form.Item>
            </Form>

        </>
    )
}

export default AddCommunitySection