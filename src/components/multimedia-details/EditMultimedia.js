import graphql from "babel-plugin-relay/macro";
import {commitMutation} from "react-relay";

const EditMultimediaMutation = graphql`
    mutation EditMultimediaMutation($multimediaId: ID!, $draft: EditMultimediaDraft!) {
        editMultimedia(multimediaId: $multimediaId, draft: $draft) {
            id
        }
    }`;

export function commitEditMultimediaMutation(
    environment,
    input,
) {

    return commitMutation(environment, {
        mutation: EditMultimediaMutation,
        variables: input,
        onCompleted: response => {
            console.log(response)

            window.location.reload();
        },
        onError: error => {console.error(error)},
    })


}