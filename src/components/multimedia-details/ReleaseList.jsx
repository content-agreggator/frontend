import React from "react";
import { List} from 'antd';
import graphql from "babel-plugin-relay/macro";
import { Typography, Divider } from 'antd';
import {Link} from "react-router-dom";

const { usePreloadedQuery, useQueryLoader,  useRelayEnvironment ,  loadQuery } = require('react-relay');

const ReleaseListQuery = graphql`
    query ReleaseListQuery($novelId: ID!) {
       releasesConnection(novelId: $novelId)  {
            edges {
                cursor
                node {
                    id
                    chapter
                    published
                    srcurl
                    volume
                }
            }
            pageInfo {
                hasNextPage
                hasPreviousPage
            }
        }
    }`;



function ReleaseListSection({novelId}) {
    const environment = useRelayEnvironment()
    const initialQueryRef = loadQuery(
        environment,
        ReleaseListQuery,
        {novelId:novelId}
    );

    return (
        <>
            {novelId != null ?
                <ReleaseList initialQueryRef={initialQueryRef}/>
                : null}

    </>
)
}

function ReleaseList({initialQueryRef}) {
    let data = usePreloadedQuery(ReleaseListQuery, initialQueryRef );

    let releases = []
    if (data.releasesConnection != null) {
        data.releasesConnection.edges.forEach(item => {releases.push(item.node)})
        releases.sort((a,b)=> {return b.chapter - a.chapter})
        console.log("releases: ", releases)
    }

    return (
        <>
            {
                releases.length > 0 ?             <List
                    size="small"

                    dataSource={releases}
                    pagination={{
                        onChange: page => {
                            console.log(page);
                        },
                        pageSize: 10,
                    }}
                    renderItem={item => (
                        <List.Item>
                            Chapter: {item.chapter} <a href={item.srcurl} >Enter</a>
                        </List.Item>
                    )}
                /> : <></> }



        </>
    )
}

export default ReleaseListSection