import {Space, Divider} from 'antd';
import { Row, Col } from 'antd';
import { Layout, Breadcrumb } from 'antd';
import CommentListSection from "../comment-section/CommentListSection";
import { Descriptions, Badge } from 'antd';
import { Typography } from 'antd';
import React from "react";
import graphql from "babel-plugin-relay/macro";
import {useParams} from "react-router-dom";
import ReleaseListSection from "./ReleaseList";
import MultimediaActions from "./MultimediaActions";
const { usePreloadedQuery,  useRelayEnvironment ,  loadQuery } = require('react-relay');
const { Text,Title } = Typography;

const { Content } = Layout;


const MultimediaDetailsQuery = graphql`
    query MultimediaDetailsQuery($id: ID!) {
        multimedia(id: $id) {
            id
            title
            alternateTitles
            authors
            description
            genres
            rating {
                avg
                count
            }
            comments {
                id
                text
                author
            }
            releases {
                chapter
                published
                srcurl
                volume
            }
            type

        }
    }`;


function MultimediaDetails() {
    // @ts-ignore
    let {novelId} = useParams()


    const environment = useRelayEnvironment()
    const initialQueryRef = loadQuery(
        environment,
        MultimediaDetailsQuery,
        {id: novelId}
    );

    console.log(initialQueryRef)


    return (
        <>
            <Space direction={"vertical"} size="large" style={{width:"100%"}}>

            <Row justify="space-around">
                <Layout>
                    <Content>
                        <Row>
                            <Col span={12}>
                                <Layout>

                                    <Row>
                                        <Divider orientation="center">Novel Info</Divider>
                                        <MultimediaInfo queryReference={initialQueryRef}/>
                                    </Row>
                                </Layout>
                            </Col>
                            <Col span={12}>
                                <Layout>
                                    <Row justify="space-around">
                                        <Divider orientation="center">Community actions</Divider>
                                        <MultimediaActions environment={environment} multimediaId={novelId}/>
                                    </Row>
                                    <Row justify="space-around">
                                        <Divider orientation="center">Releases list</Divider>
                                        <ReleaseListSection novelId={novelId}/>
                                    </Row>
                                </Layout>
                            </Col>
                        </Row>

                    </Content>

                </Layout>


            </Row>
            <Row justify="space-around">

                <Col span={24}>
                    <Layout>
                    </Layout>
                </Col>

            </Row>

            <Row justify="space-around">

                <Col span={24}>
                    <Layout>
                        <Content style={{ padding: '5px'   }}><CommentListSection novelId={novelId}/></Content>
                    </Layout>
                </Col>

            </Row>
            </Space>

        </>
)
}


function MultimediaInfo({queryReference}) {
    let data = usePreloadedQuery(MultimediaDetailsQuery, queryReference);

    const multimedia = data.multimedia

    console.log("multimedia query", multimedia)
    return (
        <>
            <Descriptions title="" layout="horizontal" bordered column={1}>
                <Descriptions.Item label="Title">{multimedia.title}</Descriptions.Item>
                <Descriptions.Item label="Alternate titles">  {multimedia.alternateTitles.map((x,i) =>
                    <Text keyboard>{x}</Text>
                ) }</Descriptions.Item>
                <Descriptions.Item label="Type">{multimedia.type}</Descriptions.Item>
                <Descriptions.Item label="Description">{multimedia.description}</Descriptions.Item>
                <Descriptions.Item label="Genres">  {multimedia.genres.map((x,i) =>
                    <Text keyboard>{x}</Text>
                    ) }</Descriptions.Item>
                <Descriptions.Item label="Authors">  {multimedia.authors.map((x,i) =>
                    <Text keyboard>{x}</Text>
                ) }</Descriptions.Item>
                <Descriptions.Item label="Rating">{"Rating: "+ multimedia.rating.avg +"   Count:"+ multimedia.rating.count}</Descriptions.Item>
            </Descriptions>
        </>
    )
}



export default MultimediaDetails