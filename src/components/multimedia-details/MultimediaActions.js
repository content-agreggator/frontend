import graphql from "babel-plugin-relay/macro";
import {loadQuery, usePreloadedQuery} from "react-relay";
import {commitJoinCommunityMutation} from "../community-section/JoinCommunity";
import {commitLeaveCommunityMutation} from "../community-section/LeaveCommunity";
import {Form, Input, Button, Checkbox, Modal, Select } from "antd";
import React from "react";
import store from "store";
import decodeJwt from "../../app/jwt";
import {commitEditMultimediaMutation} from "./EditMultimedia";

const { Option } = Select;

function MultimediaActionsSection({environment, multimediaId}) {




    // console.log(initialQueryRef)


    const token = store.get('token')
    const user = decodeJwt(token)



    return (
        <>

            <EditMultimediaModal user={user} environment={environment} multimediaId={multimediaId}/>
            {/*{initialQueryRef != null ?*/}
            {/*    // <JoinSectionCall environment={environment} communityId={communityId} queryReference={initialQueryRef}/>*/}
            {/*    : null}*/}

        </>
    )
}

function EditMultimediaModal({user,environment, multimediaId}) {

    const [visible, setVisible] = React.useState(false);
    const [confirmLoading, setConfirmLoading] = React.useState(false);
    const [modalText, setModalText] = React.useState('Content of the modal');
    const [form] = Form.useForm();

    const showEditModal = () => {
        setVisible(true);
    };

    const handleOk = () => {
        form.submit();

        setModalText('The modal will be closed after two seconds');
        setConfirmLoading(true);
        setTimeout(() => {
            setVisible(false);
            setConfirmLoading(false);
        }, 2000);
    };

    const handleCancel = () => {
        console.log('Clicked cancel button');
        setVisible(false);
    };

    const onFinish = (values) => {
        console.log('sucess:', values)

        const input = {
            multimediaId: multimediaId,
            draft: {
                title: values.title,
                alternateTitles: values.alternate_titles.replace(/\s/g , '').split(","),
                description: values.description,
                type: values.type,
                genres: values.genres.replace(/\s/g , '').split(","),
                authors: values.authors.replace(/\s/g , '').split(",")
            },
        }
        console.log(2, input)
        commitEditMultimediaMutation(environment, input)
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };



    return (
        <>
            {user.Role === "RoleAdmin" ?
                <Button onClick={showEditModal} >Edit multimedia</Button>
                : <></>
            }
            <Modal
                destroyOnClose
                title="Edit multimedia"
                visible={visible}
                onOk={handleOk}
                confirmLoading={confirmLoading}
                onCancel={handleCancel}
            >
                <Form
                    form={form}
                    preserve={false}
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        label="Title"
                        name="title"
                        initialValue={""}
                        rules={[{ required: false, message: 'Please input your title!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="Alternate Titles"
                        name="alternate_titles"
                        initialValue={""}
                        rules={[{ required: false}]}
                    >
                        <Input defaultValue={""} placeholder={'Split with ,'}/>
                    </Form.Item>

                    <Form.Item
                        label="Description"
                        name="description"
                        initialValue={""}
                        rules={[{ required: false, message: 'Please input your description!' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        name="type"
                        label="Type"
                        initialValue={""}
                        rules={[{ required: false }]}>
                        <Select
                            placeholder="Select a type"
                            allowClear
                        >
                            <Option value="novel">Novel</Option>
                            <Option value="manga">Manga</Option>
                            <Option value="anime">Anime</Option>
                        </Select>
                    </Form.Item>

                    <Form.Item
                        label="Genres"
                        name="genres"
                        initialValue={""}
                        rules={[{ required: false}]}
                    >
                        <Input  placeholder={'Split with ,'}/>
                    </Form.Item>

                    <Form.Item
                        label="Authors"
                        name="authors"
                        initialValue={""}
                        rules={[{ required: false }]}
                    >
                        <Input defaultValue={""} placeholder={ 'Split with ,'} />
                    </Form.Item>

                    {/*<Form.Item wrapperCol={{ offset: 8, span: 16 }}>*/}
                    {/*    <Button type="primary" htmlType="submit">*/}
                    {/*        Submit*/}
                    {/*    </Button>*/}
                    {/*</Form.Item>*/}
                </Form>

            </Modal>
        </>
    )

}

// function JoinSectionCall({environment, communityId, queryReference}) {
//     let data = usePreloadedQuery(ActionsSectionQuery, queryReference );
//     console.log(1, data)
//
//     const joinCommunity = () => {
//         commitJoinCommunityMutation(environment, {communityId: communityId})
//     }
//     const leaveCommunity = () => {
//         commitLeaveCommunityMutation(environment, {communityId: communityId})
//
//     }
//
//     return (
//         <>
//             { !data.isUserInCommunity ?
//                 <Button onClick={joinCommunity} >Join community</Button>
//                 :  <Button onClick={leaveCommunity} >Leave community</Button>
//             }
//
//
//
//         </>
//     )
// }

export default MultimediaActionsSection