import {Link, Redirect} from "react-router-dom";
import {Button, Checkbox, Form, Input} from "antd";
import {LockOutlined, UserOutlined} from "@ant-design/icons";
import React, {useState} from "react";
import {commitMutation, useRelayEnvironment} from "react-relay";
import graphql from "babel-plugin-relay/macro";
import store from "store";

const AddMultimediaSectionMutation = graphql`
    mutation AddMultimediaSectionMutation($title: String!, $description: String!, $authors: [String]!, $genres: [String]!) {
        addMultimedia(draft: {title: $title, description: $description, authors: $authors, genres: $genres }) {
            id
        }
    }`;

function commitAddMultimediaSectionMutation(
    environment,
    input,
    setRedirect,
) {

    return commitMutation(environment, {
        mutation: AddMultimediaSectionMutation,
        variables: input,
        onCompleted: response => {
            console.log(response)
            setRedirect(true)
        },
        onError: error => {console.error(error)},
    })


}

function AddMultimediaSection() {
    const environment = useRelayEnvironment()

    const [redirect, setRedirect] = useState(false)

    const onFinish = (values) => {
        commitAddMultimediaSectionMutation(environment, {title: values.title, description: values.description, authors: [], genres: [] }, setRedirect);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <>
            {redirect === true ? <Redirect to={'/novel-list'}/> : <></>}

            <Form
                name="normal_login"
                className="new-novel-form"
                initialValues={{ remember: true }}
                onFinish={onFinish}
            >
                <Form.Item
                    name="title"
                    rules={[{ required: true, message: 'Please input novel title' }]}
                >
                    <Input placeholder='Title' />
                </Form.Item>
                <Form.Item
                    name="description"
                    rules={[{ required: true, message: 'Please input novel description!' }]}
                >
                    <Input
                        type="description"
                        placeholder="Description"
                    />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Create community
                    </Button>
                </Form.Item>
            </Form>

        </>
    )
}

export default AddMultimediaSection