import { Table, Button } from 'antd';
import React from "react";
import {
Link, Redirect
} from "react-router-dom";

import graphql from 'babel-plugin-relay/macro';
import decodeJwt from "../../app/jwt";
let store = require('store')

const { usePreloadedQuery, useQueryLoader,  useRelayEnvironment ,  loadQuery } = require('react-relay');


const MultimediaTableQuery = graphql`
    query MultimediaTableQuery($first: Int!) {
     multimediasConnection(first: $first) {
        edges {
            cursor
            node {
                id
                description
                type
                title
            }
        }
        pageInfo {
            hasNextPage
            hasPreviousPage
        }
    }
}`;



const columns = [
    {
        title: 'Id',
        dataIndex: ['node', 'id'],
        key: 'id',
    },
    {
        title: 'Title',
        dataIndex: ['node', 'title'],
        key: 'title',
    },
    {
        title: 'Type',
        dataIndex: ['node', 'type'],
        key: 'type',
        filters: [
            {
                text: 'Novel',
                value: 'Novel',
            },
            {
                text: 'Manga',
                value: 'Manga',
            },
        ],
        onFilter: (value, record) =>
            record.node.type?.startsWith(value) ,
        filterSearch: true,
        width: '40%',

    },
    // {
    //     title: 'Address',
    //     dataIndex: ['node', 'address'],
    //     key: 'address',
    // },
    {
        title: 'Action',
        dataIndex: '',
        key: 'x',
        render:  record  => (
            <>
                <Link to={"/multimedia-details/" + record.cursor} >Enter</Link>
            </>
        )
    },
];


function MultimediaTable() {
    const environment = useRelayEnvironment()
    const initialQueryRef = loadQuery(
        environment,
        MultimediaTableQuery,
        {first: 10000}
    );

    const [queryReference, loaderQuery] = useQueryLoader(
        MultimediaTableQuery,
        initialQueryRef
    );

    return (
        <>
            {queryReference != null ?
                    <Novels queryReference={queryReference}/>
            : null}


        </>


    )
}
function Novels({queryReference}) {
    let data = usePreloadedQuery(MultimediaTableQuery, queryReference );

    const token = store.get('token')
    const user = decodeJwt(token)

    console.log(user)


    return (
        <>
            <Table
                rowKey={record => record.cursor}
                columns={columns}
                   dataSource={data.multimediasConnection.edges}
                   expandable={{
                       expandedRowRender: record => <p style={{ margin: 0 }}><code> {record.node?.description} </code> </p>,
                       rowExpandable: record => record.cursor !== "",
                   }}
            />
            {user.Role === "RoleAdmin" ?
                <Button href={"/new-multimedia"}>Add multimedia</Button>
                : <></>
            }

        </>
    );
}




export default MultimediaTable