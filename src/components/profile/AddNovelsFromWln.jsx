import graphql from "babel-plugin-relay/macro";
import {commitMutation} from "react-relay";

const AddNovelsFromWlnMutation = graphql`
    mutation AddNovelsFromWlnMutation($from: Int!, $to: Int!) {
        addNovelsFromWLN(from: $from, to:$to) {
            id
        }
    }`;

export function commitAddNovelsFromWlnMutation(
    environment,
    input,
) {

    return commitMutation(environment, {
        mutation: AddNovelsFromWlnMutation,
        variables: input,
        onCompleted: response => {
            console.log(response)
            window.location.reload();
        },
        onError: error => {console.error(error)},
    })


}
