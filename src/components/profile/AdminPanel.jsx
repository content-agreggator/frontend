
import { Form, Input, Select, Tooltip, Button, Space, Typography } from 'antd';

import store from "store";
import decodeJwt from "../../app/jwt";

import React from "react";
import {commitAddNovelsFromWlnMutation} from "./AddNovelsFromWln";

function AdminPanel({environment, userId}) {
    // @ts-ignore
    // let {novelId} = useParams()



    return (
        <>
            <AddNovelsFromWlnCall environment={environment}/>
        </>
    )
}

function AddNovelsFromWlnCall({environment, userOther}) {

    const token = store.get('token')
    const user = decodeJwt(token)

    console.log("user", user)


    console.log("213", userOther)

    const onFinish = values => {
        commitAddNovelsFromWlnMutation(environment, {from: values.from, to: values.to})
    }


    return (
        <>

                <>
                    <Form name="complex-form" onFinish={onFinish} labelCol={{ span: 8 }} wrapperCol={{ span: 16 }}>
                        <Form.Item label="Get novels from WLN 3rd API ids" style={{ marginBottom: 0 }}>
                            <Form.Item
                                name="from"
                                rules={[{ required: true }]}
                                style={{ display: 'inline-block', width: 'calc(50% - 8px)' }}
                            >
                                <Input placeholder="From id" />
                            </Form.Item>
                            <Form.Item
                                name="to"
                                rules={[{ required: true }]}
                                style={{ display: 'inline-block', width: 'calc(50% - 8px)', margin: '0 8px' }}
                            >
                                <Input placeholder="To id" />
                            </Form.Item>
                        </Form.Item>
                        <Form.Item label="" colon={false}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </>
        </>
    )
}

export default AdminPanel