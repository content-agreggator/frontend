import {useParams} from "react-router-dom";
import {loadQuery, useRelayEnvironment} from "react-relay";
import {Col, Descriptions, Divider, Layout, Row, Space} from "antd";
import React from "react";

import { Typography } from 'antd';
import graphql from "babel-plugin-relay/macro";
import ProfileDetails, {ProfileDetailsQuery} from "./ProfileDetails";
import decodeJwt from "../../app/jwt";
import store from "store";
import AdminPanel from "./AdminPanel";
const { usePreloadedQuery  } = require('react-relay');
const { Title } = Typography;

const { Content } = Layout;

function ProfileSection() {
    // @ts-ignore
    let {userId} = useParams()
    console.log(userId)

    const token = store.get('token')
    const user = decodeJwt(token)

    console.log(user)

    const environment = useRelayEnvironment()
    const initialQueryRef = loadQuery(
        environment,
        ProfileDetailsQuery,
        {}
    );

    return (
        <>
            <Space direction={"vertical"} size="large" style={{width:"100%"}}>

                <Row justify="space-around">
                    <Layout>
                        <Content>
                            <Row>
                                <Col span={12}>
                                    <Layout>

                                        <Row>
                                            <Divider orientation="center">Profile Details</Divider>
                                            <ProfileDetails queryReference={initialQueryRef}/>
                                        </Row>
                                    </Layout>
                                </Col>
                                <Col span={12}>
                                    <Layout>
                                        <Row justify="space-around">
                                            { user.Role == "Admin" ?
                                                <>
                                                    <Divider orientation="center">Panel admin</Divider>
                                                    <AdminPanel environment={environment} userId={userId}/>
                                                </> : <></> }
                                        </Row>
                                        <Row>
                                            <Divider orientation="center">Friends List</Divider>
                                            {/*<ReleaseListSection/>*/}
                                        </Row>

                                    </Layout>
                                </Col>
                            </Row>

                        </Content>

                    </Layout>


                </Row>
                <Row justify="space-around">

                    <Col span={24}>
                        <Layout>
                        </Layout>
                    </Col>

                </Row>

                <Row justify="space-around">

                    <Col span={24}>
                        <Layout>
                            {/*<Content style={{ padding: '5px'   }}><CommentListSection novelId={novelId}/></Content>*/}
                        </Layout>
                    </Col>

                </Row>
            </Space>

        </>
    )
}


export default ProfileSection