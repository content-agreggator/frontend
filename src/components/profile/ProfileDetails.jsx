import {Link, useParams} from "react-router-dom";
import {loadQuery, useRelayEnvironment} from "react-relay";
import {Col, Descriptions, Divider, Layout, List, Row, Space} from "antd";
import React from "react";

import { Typography } from 'antd';
import graphql from "babel-plugin-relay/macro";
const { usePreloadedQuery  } = require('react-relay');
const { Title } = Typography;

const { Content } = Layout;

export const ProfileDetailsQuery = graphql`
    query ProfileDetailsQuery {
        user {
            id
            username
            email
            friends {
                username
            }
            role
        }
    }`;

function ProfileDetails({queryReference}) {
    let data = usePreloadedQuery(ProfileDetailsQuery, queryReference);
    console.log(data)
    const user = data.user
    return (
        <>
            <Descriptions title="" layout="horizontal" bordered column={1}>
                <Descriptions.Item label="Username">{user.username}</Descriptions.Item>
                <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                <Descriptions.Item label="Role">{user.role}</Descriptions.Item>
                {/*<Descriptions.Item label="Rating">{novel.rating.avg +"  "+ novel.rating.count}</Descriptions.Item>*/}
            </Descriptions>
        </>
    )
}

function FriendsList({users}) {

    console.log(users)
    return (
        <>
            {/*<Descriptions title="" layout="horizontal" bordered column={1}>*/}
            {/*    <Descriptions.Item label="Name">{community.name}</Descriptions.Item>*/}
            {/*    <Descriptions.Item label="Description">{community.description}</Descriptions.Item>*/}
            {/*</Descriptions>*/}
            {
                users?.length > 0 ?             <List
                    size="small"

                    dataSource={users}
                    pagination={{
                        onChange: page => {
                            console.log(page);
                        },
                        pageSize: 10,
                    }}
                    renderItem={item => (
                        <List.Item>
                            {item.userName} {item.role} <Link to={"/user/"+ item.userId} >Enter</Link>
                        </List.Item>
                    )}
                /> : <></> }
        </>
    )
}

export default ProfileDetails