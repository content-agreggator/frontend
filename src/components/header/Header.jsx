import {Menu, Layout} from 'antd';
import React from "react";
import {Link,  useLocation} from "react-router-dom";
import { Input,  } from 'antd';
import decodeJwt from "../../app/jwt";

const { Search } = Input;

const { Header } = Layout;
let store = require('store')



function HeaderCustom() {
    const location = useLocation();

    const onSearch = value => console.log(value);

    const token = store.get('token')
    const user = decodeJwt(token)

    console.log(user)

    return (
        <>
            <Header className="header">
                {/*<div className="logo"> Content Aggregator </div>*/}
                {/*<div className="logo" >Logo</div>*/}
                <Menu theme="dark"  mode="horizontal" defaultSelectedKeys={[location.pathname]}>
                    <Menu.Item key="/"> <Link to="/"> Main page</Link></Menu.Item>
                    <Menu.Item key="/multimedia-list"><Link to="/multimedia-list">Multimedia list</Link></Menu.Item>
                    <Menu.Item key="/community-list"><Link to="/community-list">Communities list</Link></Menu.Item>
                    {token == null ?
                        <>
                            <Menu.Item key="/login"><Link to="/login">Login</Link></Menu.Item>
                            <Menu.Item key="/register"><Link to="/register">Register</Link></Menu.Item>
                        </>
                        :
                        <>
                            <Menu.Item key="/profile"> <Link to="/profile">Profile</Link></Menu.Item>
                            <Menu.Item key="/logout"><Link to="/logout">Log out</Link></Menu.Item>
                        </>
                    }
                    <Menu.Item key="/search"><Link to="/search">Search</Link></Menu.Item>
                    <Menu.Item><Search placeholder="Search for multimedia" onSearch={onSearch} enterButton /></Menu.Item>
                </Menu>
            </Header>

        </>

    )
}

export default HeaderCustom