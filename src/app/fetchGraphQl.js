var store = require('store')

async function fetchGraphQL(text, variables) {
    const response = await fetch('http://localhost:8080/query', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Authorization': `Bearer `+ store.get('token'),
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: text,
            variables,
        }),
    });

    // Get the response as JSON
    return response.json();
}

export default fetchGraphQL;
