import {Environment, Network, RecordSource, Store, Observable} from 'relay-runtime';
import fetchGraphQL from './fetchGraphQl';

// Relay passes a "params" object with the query name and text. So we define a helper function
// to call our fetchGraphQL utility with params.text.
async function fetchRelay(params, variables) {
    console.log(`fetching query ${params.name} with ${JSON.stringify(variables)}`);

    const rsp = await fetchGraphQL(params.text, variables)

    if (Array.isArray(rsp.errors)) {
        console.log(rsp.errors);
        throw new Error(
            `Error fetching GraphQL query '${
                params.name
            }' with variables '${JSON.stringify(variables)}': ${JSON.stringify(
                rsp.errors,
            )}`,
        );
    }

    return rsp;
}

// Export a singleton instance of Relay Environment configured with our network function:
export default new Environment({
    network: Network.create(fetchRelay),
    store: new Store(new RecordSource(), {    gcReleaseBufferSize: 10, fetchPolicy: "store-and-network"}),
});
