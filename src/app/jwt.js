import jwt_decode from "jwt-decode";

export default function decodeJwt(token) {
    if (token != null) {
        return jwt_decode(token)
    }
    return {}
}
