import React from 'react';
import logo from './logo.svg';
import './App.scss';
import 'antd/dist/antd.css';
import './index.css';
import { Layout, Breadcrumb } from 'antd';
import HeaderCustom from "./components/header/Header";
import MainContent from "./components/main-content/MainContent";
import jwt_decode from "jwt-decode";
import decodeJwt from "./app/jwt";

const { loadQuery, useRelayEnvironment} = require('react-relay');
let store = require('store')




function App() {

  const token = decodeJwt(store.get('token'))

    const expireTime = new Date(token.exp)
    if (expireTime - 10000 > Date.now()) {
      store.remove('token')
    }

  return (
    <div className="App">
      <Layout>
        <HeaderCustom/>
        <MainContent/>
      </Layout>
    </div>
  );
}

export default App;
